<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../public/books.ico" />

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Gestión de Bibliotecarios</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>

                    @if(Session::has('Mensaje'))
                    
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('Mensaje')  }}
                    </div>
                        @endif

<div class="table-responsive">

<div class="col-5">
<div class="input-group mb-2">
  <input id="texto" type="text" class="form-control" placeholder="Buscar palabra clave" aria-label="Recipient's username" aria-describedby="basic-addon2">
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">Buscar</span>
  </div>&nbsp;&nbsp;
  <input type="button" onclick="#"
     class="btn btn-secondary" value="Filtro de Búsqueda">
</div>
</div>


<table class="table table-light table-hover">
    <caption>Información de Bibliotecarios</caption>
    <thead class="thead-light">
        <tr>
            <th id="Consecutivo">#</th>
            <th id="Documento">Documento</th>
            <th id="NombreCompleto">Nombre completo</th>
            <th id="Sexo">Sexo</th>
            <th id="Correo">Correo</th>
            <th id="Direccion">Dirección</th>
            <th id="Acciones">Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($empleados as $empleado)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $empleado->Documento }}</td>
            <td>{{ $empleado->Nombre }} {{ $empleado->Apellidos }}</td>
            <td>{{ $empleado->Sexo }}</td>
            <td>{{ $empleado->Correo }}</td>
            <td>{{ $empleado->Direccion }}</td>
          <!--  <td>
            <img src="{{ asset('storage').'/'.$empleado->Foto }}" alt="" width="100">

            </td> --> 
            <td> 
            
            <a class="btn btn-warning" href="{{ url('/empleados/'.$empleado->id.'/edit') }}">Editar</a>
            
            <form method="post" action="{{ url('/empleados/'.$empleado->id) }}" style="display:inline">
            {{ csrf_field() }}
            {{ method_field('DELETE') }} <!-- IDENTIFICADOR para llamar al método destroyer -->
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Inactivar Bibliotecario?');">Inactivar</button>
               
            </form></td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $empleados->links() }} <!-- Páginado -->

</div>
<input type="button" onclick="location.href = '{{ url('empleados/create') }}'"
     class="btn btn-primary" value="Añadir bibliotecario">
<input type="button" onclick="location.href = '{{ route('home') }}'"
     class="btn btn-secondary" value="Regresar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

