<?php

namespace App\Http\Controllers;

use App\Empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage; //Para borrar registros de la carpeta Storage>>uploads

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['empleados']=Empleados::paginate(5); //Mostrar tabla de 5 registros páginada.
        return view('empleados.index', $datos); //Datos enviados a la Vista
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('empleados.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $campos=[
            'Documento'=> 'required|string|max:15',
            'Nombre'=> 'required|string|max:100',
            'Apellidos'=> 'required|string|max:100',
            'Sexo'=> 'required|string|max:1',
            'Telefono'=> 'required|string|max:10',
            'Correo'=> 'required|email',
            'Direccion'=> 'required|string|max:100',
            'Clave'=> 'required|string|max:100|min:8'
        ];
        $Mensaje=["required"=>'El campo :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);

        // $datosEmpleado = request()->all(); Todo lo enviado al método se almacena en la variable.
        
        $datosEmpleado = request()->except('_token'); 
        
        if($request->hasFile('Foto')){
            $datosEmpleado['Foto']=$request->file('Foto')->store('uploads', 'public');
        }

        Empleados::insert($datosEmpleado);

        //return response()->json($datosEmpleado);
        return redirect('empleados')->with('Mensaje', 'Bibliotecario agregado con éxito'); //Redireccionando a la ruta inicial
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(Empleados $empleados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empleado = Empleados::findOrFail($id);

        return view('empleados.edit', compact('empleado'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $campos=[
            'Documento'=> 'required|string|max:15',
            'Nombre'=> 'required|string|max:100',
            'Apellidos'=> 'required|string|max:100',
            'Sexo'=> 'required|string|max:1',
            'Telefono'=> 'required|string|max:10',
            'Correo'=> 'required|email',
            'Direccion'=> 'required|string|max:100',
            'Clave'=> 'required|string|max:100|min:8'
        ];
        $Mensaje=["required"=>'El campo :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);


        //
        $datosEmpleado = request()->except(['_token', '_method']); 

        if($request->hasFile('Foto')){
            $empleado = Empleados::findOrFail($id);

            Storage::delete('public/'.$empleado->Foto); // Se borra la fotografía antigua.

            $datosEmpleado['Foto']=$request->file('Foto')->store('uploads', 'public');
        }

        Empleados::where('id','=',$id)->update($datosEmpleado);

        //$empleado = Empleados::findOrFail($id);
        //return view('empleados.edit', compact('empleado'));
        return redirect('empleados')->with('Mensaje', 'Bibliotecario modificado con éxito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $empleado = Empleados::findOrFail($id);

      //   if(Storage::delete('public/'.$empleado->Foto)){
            Empleados::destroy($id); //Eliminar el registro en base al id enviado por la vista.
        //   } 

        return redirect('empleados')->with('Mensaje', 'Bibliotecario eliminado con éxito');; //Redireccionando a la ruta inicial

    }
}
